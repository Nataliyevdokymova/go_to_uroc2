import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:go_to_uroc2/pages/home.dart';
import 'package:go_to_uroc2/pages/main_screen.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  theme: ThemeData(
    primaryColor: Colors.deepOrangeAccent,
  ),
  initialRoute: '/',
  routes: {
    '/': (context)=> MainScreen(),
    '/goto': (context)=> Home(),
  },

));